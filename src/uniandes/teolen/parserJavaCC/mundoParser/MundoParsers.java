package uniandes.teolen.parserJavaCC.mundoParser;

import java.util.*;

import uniandes.teolen.parserJavaCC.ExpRegs.ExpRegs;
import uniandes.teolen.parserJavaCC.ParserProyectoAlad.ParserAlad;
import uniandes.teolen.parserJavaCC.ParserProyectoAnderson.ParserAnderson;


public class MundoParsers {
	
	// Nombres de los Parsers
	private  ArrayList  <String> parsers  = new ArrayList <> (); 
	
	// Parser que se est� usando
	private int currentParser;
	
	
	public  MundoParsers () {
  	
		// Agreguen al final de esta lista los nombres del nuevo parser
		

	    parsers.add("Expreciones Regulares");
	    parsers.add("Proyecto-Alad");
	    parsers.add("Proyecto-Anderson");
	    
	    currentParser =  0;

	}

	private ExpRegs getExpereg()
	{
		return new ExpRegs(System.in);
	}
    private ParserAlad getAlad()
    {
        return new ParserAlad(System.in);
    }
    private ParserAnderson getAnderson()
    {
        return new ParserAnderson(System.in);
    }


	
	
	public String getStringCurrentParser(){
		return parsers.get(currentParser);
	}
	
	public void setCurrentParser(int p) {
		currentParser = p;
	}
	
	public String getParser(int i) {
		return parsers.get(i);
	}
	
	public int sizeParsers() {
		return parsers.size();
	}
	
	public String  procesarCadena(String texto) {
		String resp = "";

		if(parsers.get(currentParser).equals("Expreciones Regulares")){
            ExpRegs nuevoParser = getExpereg();
            nuevoParser.ReInit(new java.io.StringReader(texto));
            try {
                nuevoParser.expsRegs();
                resp = new String("OK   \n");
            }catch (Exception e) {
                resp = new String ("Error de Sintaxis: "+e.getMessage());
            } catch (Error e) {
                resp = new String ("Error Lexico: "+e.getMessage());
            }
        }
		else if(parsers.get(currentParser).equals("Proyecto-Alad")){
            ParserAlad nuevoParser = getAlad();
            nuevoParser.ReInit(new java.io.StringReader(texto));
            try {
                nuevoParser.clase();
                resp = new String("OK   \n");
            }catch (Exception e) {
                resp = new String ("Error de Sintaxis: "+e.getMessage());
            } catch (Error e) {
                resp = new String ("Error Lexico: "+e.getMessage());
            }
        }
        else if(parsers.get(currentParser).equals("Proyecto-Anderson")){
            ParserAnderson nuevoParser = getAnderson();
            nuevoParser.ReInit(new java.io.StringReader(texto));
            try {
                nuevoParser.clase();
                resp = new String("OK   \n");
            }catch (Exception e) {
                resp = new String ("Error de Sintaxis: "+e.getMessage());
            } catch (Error e) {
                resp = new String ("Error Lexico: "+e.getMessage());
            }
        }
		return "\n SISTEMA " + parsers.get(currentParser) + ": " + resp + "\n";
	}

}
