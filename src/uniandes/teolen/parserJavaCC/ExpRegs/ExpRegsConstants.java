/* Generated By:JavaCC: Do not edit this line. ExpRegsConstants.java */
package uniandes.teolen.parserJavaCC.ExpRegs;


/**
 * Token literal values and constants.
 * Generated by org.javacc.parser.OtherFilesGen#start()
 */
public interface ExpRegsConstants {

  /** End of File. */
  int EOF = 0;
  /** RegularExpression Id. */
  int MINUS = 3;
  /** RegularExpression Id. */
  int MULTIPLY = 4;
  /** RegularExpression Id. */
  int SPACE = 5;
  /** RegularExpression Id. */
  int PLUS = 6;
  /** RegularExpression Id. */
  int EQUALS = 7;
  /** RegularExpression Id. */
  int DOTS = 8;
  /** RegularExpression Id. */
  int INI = 9;
  /** RegularExpression Id. */
  int END = 10;
  /** RegularExpression Id. */
  int SEMICOLON = 11;
  /** RegularExpression Id. */
  int COMA = 12;
  /** RegularExpression Id. */
  int OR = 13;
  /** RegularExpression Id. */
  int INITCOURV = 14;
  /** RegularExpression Id. */
  int ENDCOURV = 15;
  /** RegularExpression Id. */
  int INITBEN = 16;
  /** RegularExpression Id. */
  int ENDBEN = 17;
  /** RegularExpression Id. */
  int LINEJUMP = 18;
  /** RegularExpression Id. */
  int VOID = 19;
  /** RegularExpression Id. */
  int ALPHABET = 20;
  /** RegularExpression Id. */
  int SYMBOL = 21;
  /** RegularExpression Id. */
  int CONSTANT = 22;
  /** RegularExpression Id. */
  int DIGIT = 23;
  /** RegularExpression Id. */
  int WORD = 24;
  /** RegularExpression Id. */
  int CHARACTER = 25;
  /** RegularExpression Id. */
  int UPCARACTER = 26;

  /** Lexical state. */
  int DEFAULT = 0;

  /** Literal token values. */
  String[] tokenImage = {
    "<EOF>",
    "\"\\r\"",
    "\"\\t\"",
    "\"-\"",
    "\"*\"",
    "\" \"",
    "\"+\"",
    "\"=\"",
    "\":\"",
    "\"BeginERS\"",
    "\"EndERS\"",
    "\";\"",
    "\",\"",
    "<OR>",
    "\"(\"",
    "\")\"",
    "\"{\"",
    "\"}\"",
    "\"\\n\"",
    "\"@\"",
    "\"Alphabet\"",
    "<SYMBOL>",
    "<CONSTANT>",
    "<DIGIT>",
    "<WORD>",
    "<CHARACTER>",
    "<UPCARACTER>",
  };

}
